// lab_1_2_3_cpp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "lab_1.h"
#include "lab_2.h"
#include "lab_3.h"

int _tmain(int argc, _TCHAR* argv[])
{
	lab::TestLab1();
	lab::TestLab2();
	lab::TestLab3();

	return 0;
}

