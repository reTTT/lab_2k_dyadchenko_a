#pragma once
#include <memory>
#include <iostream>

namespace lab
{
	template<class Tk, class Td>
	class BinaryTree
    {
	public:
        
    	class Node
        {
		public:
			
			Node(Tk key, Td data) : key(key), data(data) {}

			Tk key;
			Td data;
            
            std::shared_ptr<Node> left;
            std::shared_ptr<Node> right;

			void Print()
			{
				std::cout << "key: " << key << " data:" << data << std::endl;
			}
        };

		typedef std::shared_ptr<Node> node_t;
    	
    	void printLKR()
    	{
    		std::cout << "Begin print LKR" << std::endl;
    		printLKR(root);
			std::cout << "End print LKR" << std::endl << std::endl;
    	}
    	
    	void printLKR(node_t& node)
    	{
    		if(node == nullptr) return;
    		
    		if(node->left != nullptr)
    		{
    			printLKR(node->left);
    		}
    		
			node->Print();
    		
    		if(node->right != nullptr)
    		{
    			printLKR(node->right);
    		}
    	}
    	
    	void printKLR()
    	{
    		std::cout << "Begin print KLR" << std::endl;
    		printKLR(root);
    		std::cout << "End print KLR" << std::endl << std::endl;
    	}
    	
    	void printKLR(node_t& node)
    	{
    		if(node == nullptr) return;
    		
    		node->Print();
    		
    		if(node->left != nullptr)
    		{
    			printKLR(node->left);
    		}
    		
    		if(node->right != nullptr)
    		{
    			printKLR(node->right);
    		}
    	}
    	
    	void printLRK()
    	{
    		std::cout << "Begin print LRK" << std::endl;
    		printLRK(root);
    		std::cout << "End print LRK" << std::endl << std::endl;
    	}
    	
    	void printLRK(node_t& node)
    	{
    		if(node == nullptr) return;
    		
    		if(node->left != nullptr)
    		{
    			printLRK(node->left);
    		}
    		
    		if(node->right != nullptr)
    		{
    			printLRK(node->right);
    		}
    		
    		node->Print();
    	}

        void Insert(Tk key, Td data)
        {
            if (root == nullptr)
            {
                root = node_t( new Node(key, data) );
            }
            else
            {
                Insert(key, data, root);
            }
        }

        void Insert(Tk key, Td data, node_t leaf)
        {
            if (key < leaf->key)
            {
                if(leaf->left != nullptr)
                    Insert(key, data, leaf->left);
                else
                    leaf->left = node_t( new Node(key, data) );
            }
            else
            {
                if(leaf->right != nullptr)
                    Insert(key, data, leaf->right);
                else
                    leaf->right = node_t( new Node(key, data) );
            }
        }

        bool ContainsKey(Tk key)
        {
            return Search(key, root) != nullptr;
        }

        node_t Search(Tk key, node_t leaf)
        {
            if(leaf != nullptr)
            {
                if(key = leaf->key)
                    return leaf;
                else if (key < leaf->key)
                    return Search(key, leaf->left);
                else
                    return Search(key, leaf->right);
            }
            else
                return node_t();
        }

        bool Empty() const
        {
			return root == nullptr;
        }
        
        void Delete(Tk key)
        {
            node_t temp = root;
            node_t parent = root;

            if (temp == nullptr)
            {
                //cout << "The tree is empty" << endl;
            }
            else
            {
                while (temp != nullptr && temp->key != key)
                {
                    parent = temp;

                    if (temp->key < key)
                    {
                        temp = temp->right;
                    }
                    else
                    {
                        temp = temp->left;
                    }
                }
            }
 
            if (temp == nullptr)
            {
                //cout << "No node present";
            }
            else if (temp == root)
            {
                if (temp->right == nullptr && temp->left == nullptr)
                {
                    root = nullptr;
                }
                else if (temp->left == nullptr)
                {
                    root = temp->right;
                }
                else if (temp->right == nullptr)
                {
                    root = temp->left;
                }
                else
                {
                    node_t temp1 = temp->right;
                   
                    while (temp1->left != nullptr)
                    {
                        temp = temp1;
                        temp1 = temp1->left;
                    }

                    if (temp1 != temp->right)
                    {
                        temp->left = temp1->right;
                        temp1->right = root->right;
                    }

                    temp1->left = root->left;
                    root = temp1;
                }
            }
            else
            {
                if (temp->right == nullptr && temp->left == nullptr)
                {
                    if (parent->right == temp)
                        parent->right = nullptr;
                    else
                        parent->left = nullptr;
                }
                else if (temp->left == nullptr)
                {
                    if (parent->right == temp)
                        parent->right = temp->right;
                    else
                        parent->left = temp->right;
                }
                else if (temp->right == nullptr)
                {
                    if (parent->right == temp)
                        parent->right = temp->left;
                    else
                        parent->left = temp->left;
                }
                else
                {
                    parent = temp;
                    node_t temp1 = temp->right;

                    while (temp1->left != nullptr)
                    {
                        parent = temp1;
                        temp1 = temp1->left;
                    }

                    if (temp1 != temp->right)
                    {
                        temp->left = temp1->right;
                        temp1->right = parent->right;
                    }

                    temp1->left = parent->left;
                    parent = temp1;
                }
            }
        }
        
	protected:

        node_t root;
    };
}