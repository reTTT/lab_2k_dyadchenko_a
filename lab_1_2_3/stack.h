#pragma once
#include <memory>

namespace lab
{
	template<class T>
	class Stack
    {
	protected:
	
		class Node
        {
		public:

			Node(const T& data): data(data) {}

            T data;
            std::shared_ptr<Node> next;
        };

		typedef std::shared_ptr<Node> node_t;

	public:

        void Push(T data)
        {
            if (Empty())
            {
                root =  node_t( new Node(data) );
            }
            else
            {
                node_t node = root;

                while (node->next != nullptr)
                {
                    node = node->next;
                }

                node->next = node_t( new Node(data) );
            }
        }

        void Pop()
        {
            if (Empty()) return;

            node_t node = root;
            
            while (node->next != nullptr && node->next->next != nullptr)
            {
                node = node->next;
            }

            if (node->next == nullptr)
                root = nullptr;
            else
                node->next = nullptr;
        }

        const T& Top() const
        {
            if (Empty())
            {
                throw("stackIsEmpty");
            }

            node_t node = root;
            while (node->next != nullptr)
            {
                node = node->next;
            }

            return node->data;
        }

        size_t Size() const
        {
            
			size_t count = 0;
            node_t node = root;

            while (node != nullptr)
			{
				++count;
                node = node->next;
            }

			return count;
            
        }

        bool Empty() const
        {
			return root == nullptr;
        }

	protected:

        node_t root;
    };
}