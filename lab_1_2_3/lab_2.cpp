#include "lab_2.h"
#include <iostream>

namespace lab
{
	void TestLab2()
	{
		std::cout << "-= Lab 2 =-" << std::endl;
		std::cout << "Begin test Stack:" << std::endl;
		
		Stack<int> stack;

		stack.Push(5);
        stack.Push(3);
        stack.Push(7);

		std::cout << "top: " << stack.Top() << std::endl;
		    
        stack.Pop();

        std::cout << "top: " << stack.Top() << std::endl;

		stack.Pop();

        std::cout << "top: " << stack.Top() << std::endl;

        stack.Pop();
		std::cout << "is empty: " << stack.Empty() << std::endl;

		std::cout << "End test Stack" << std::endl;
		std::cout << "End test Lab 2" << std::endl << std::endl;
	}
}