#pragma once
#include <memory>

namespace lab
{
	template<class T>
	class ILinkedList
    {
	public:
        virtual void Push(T data) = 0;
        virtual void Pop() = 0;

        virtual void InsertBefore(size_t idx, T data) = 0;
        virtual void InsertAfter(size_t idx, T data) = 0;
        virtual void Remove(size_t idx) = 0;
        virtual T Get(size_t idx) const = 0;

        virtual size_t Size() const = 0;
        virtual bool Empty() const = 0;
    };


	template<class T>
	class LinkedList : public ILinkedList<T>
    {
	protected:

		class Node
        {
		public:

			Node(T data): data(data) {}

            T data;
			std::shared_ptr<Node> next;
        };

	public:

		typedef std::shared_ptr<Node> node_t;
        
        void Push(T data)
        {
            if (Empty())
            {
                root = node_t( new Node(data) );
            }
            else
            {
                node_t old = root;
                
				while (old->next != nullptr)
                {
                    old = old->next;
                }

                old->next = node_t( new Node(data) );
            }
        }

        void Pop()
        {
            if (Empty()) return;

            node_t old = root;
            while (old->next != nullptr && old->next->next != nullptr)
            {
                old = old->next;
            }

            if (old->next == nullptr)
                root = nullptr;
            else
                old->next = nullptr;
        }
        
		void InsertBefore(size_t idx, T data)
        {
			if (idx >= Size())
			{
				throw("indexOutOfRange");
			}
                
			size_t count = 0;
			node_t old = nullptr;
			node_t cur = root;

			while (idx != count)
			{
				++count;
				old = cur;
				cur = cur->next;
			}

			node_t node = node_t( new Node(data) );

			if (old == nullptr)
			{
				node->next = cur;
				root = node;
			}
			else
			{
				old->next = node;
				node->next = cur;
			}
        }

        void InsertAfter(size_t idx, T data)
        {
			if (idx >= Size())
			{
				throw("indexOutOfRange");
			}
                
			size_t count = 0;
			node_t cur = root;

			while (idx != count)
			{
				++count;
				cur = cur->next;
			}

			node_t node = node_t( new Node(data) );
			node->next = cur->next;
            cur->next = node;
        }

        void Remove(size_t idx)
        {
            if (idx >= Size())
            {
                throw("indexOutOfRange");
            }

            size_t count = 0;
            node_t old = nullptr;
            node_t cur = root;

            while (idx != count)
            {
                ++count;
                old = cur;
                cur = cur->next;
            }

            if (old != nullptr)
            {
                if (cur != nullptr)
                    old->next = cur->next;
                else
                    old->next = nullptr;
            }
            else
            {
                if (root->next != nullptr)
                    root = root->next;
                else
                    root = nullptr;
            }
        }

		T Get(size_t idx) const
        {
            if (idx >= Size())
            {
				throw("indexOutOfRange");
            }

            size_t count = 0;
            node_t node = root;

            while (idx != count)
            {
                ++count;
                node = node->next;
            }

            return node->data;
        }

        size_t Size() const
        {
            
			size_t count = 0;
            node_t node = root;

            while (node != nullptr)
			{
				++count;
                node = node->next;
            }

			return count;
            
        }

        bool Empty() const
        {
			return root == nullptr;
        }

	protected:

        node_t root;
    };
}