#include "lab_3.h"
#include <iostream>
#include "tree.h"
#include <string>

namespace lab
{
	void TestLab3()
	{
		std::cout << "-= Lab 3 =-" << std::endl;
		std::cout << "Begin test Tree:" << std::endl;

		BinaryTree<std::string, std::string> tree;

		tree.Insert("Nikolaev", "2894");
		tree.Insert("Neiman", "2894");
		tree.Insert("Dyadchenko", "2894");
		tree.Insert("Chapligin", "2894");
		tree.Insert("Zaiceva", "2894");

		tree.Insert("Aleksandrov", "2893");
		tree.Insert("Gusev", "2893");
		tree.Insert("Gladnikov", "2893");

		tree.Delete("Zaiceva");
            
		tree.printLKR();
		tree.printKLR();
		tree.printLRK();

		std::cout << "End test Tree" << std::endl;

		std::cout << "End test Lab 3" << std::endl << std::endl;
	}
}