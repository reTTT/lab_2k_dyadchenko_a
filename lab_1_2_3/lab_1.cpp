#include "lab_1.h"
#include <iostream>

namespace lab
{
	template<class T>
	void PrintIntList(const ILinkedList<T>& l)
    {
		size_t count = l.Size();

		std::cout << "begin print:" << std::endl;

		for (size_t i = 0; i < count; ++i)
		{
			std::cout << l.Get(i) << "; ";
		}

		std::cout << std::endl << "end." << std::endl;
	}

	template<class T>
	void TestList(ILinkedList<T>& list1)
    {
		try
		{
			list1.InsertAfter(0, -1);
		}
		catch(const char* e)
		{
			std::cout << "insert in empty list. exception: " << e << std::endl;
		}
		
		
		for (int i = 0; i < 10; ++i)
        {
			list1.Push(i);
        }

		list1.InsertBefore(7, 77);
		PrintIntList<T>(list1);

		list1.InsertBefore(9, 88);
		PrintIntList<T>(list1);

		list1.InsertBefore(0, -10);
		PrintIntList<T>(list1);

		list1.InsertBefore(2, 11);
		PrintIntList<T>(list1);

		list1.InsertAfter(list1.Size() - 1, 1005);
		PrintIntList<T>(list1);

		list1.InsertAfter(0, -5);
		PrintIntList<T>(list1);

		list1.InsertAfter(8, 55);
		PrintIntList<T>(list1);

		list1.Remove(15);

		PrintIntList<T>(list1);

		list1.Remove(9);

		PrintIntList<T>(list1);

		list1.Remove(0);

		PrintIntList<T>(list1);

		list1.Remove(4);

		PrintIntList<T>(list1);

		list1.Pop();
		list1.Pop();

		PrintIntList<T>(list1);

		list1.Pop();
		list1.Pop();

		PrintIntList<T>(list1);
	}
	
	void TestLab1()
	{
		std::cout << "-= Lab 1 =-" << std::endl;
		
		std::cout << "Begin test List:" << std::endl;
		TestList<int>(LinkedList<int>());
		std::cout << "End test List" << std::endl << std::endl;
		
		std::cout << "Begin test Double List:" << std::endl;
		TestList<int>(DoubleLinkedList<int>());
		std::cout << "End test Double List" << std::endl;

		std::cout << "End test Lab 1" << std::endl << std::endl;

	}
}