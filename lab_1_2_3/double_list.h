#pragma once
#include "list.h"

namespace lab
{
	template<class T>
	class DoubleLinkedList : public ILinkedList<T>
    {
	protected:

		class Node
        {
		public:
			
			Node(T data): data(data) {}

            T data;

            std::shared_ptr<Node> prev;
            std::shared_ptr<Node> next;
        };

		typedef std::shared_ptr<Node> node_t;

	public:

        void Push(T data)
        {
            if (Empty())
            {
                root = node_t( new Node(data) );
            }
            else
            {
                node_t cur = root;
                
				while (cur->next != nullptr)
                {
                    cur = cur->next;
                }

                cur->next = node_t( new Node(data) );
                cur->next->prev = cur;
            }
        }

        void Pop()
        {
            if (Empty()) return;

            node_t cur = root;
            
			while (cur->next != nullptr)
            {
                cur = cur->next;
            }

            if (cur->prev != nullptr)
                cur->prev->next = nullptr;
            else
                root = nullptr;
        }

        void InsertBefore(size_t idx, T data)
        {
            if (idx >= Size())
			{
				throw("indexOutOfRange");
			}

			size_t count = 0;
			node_t cur = root;

			while (idx != count)
			{
				++count;
				cur = cur->next;
			}

			node_t node = node_t( new Node(data) );

			if (cur->prev == nullptr)
			{
				node->next = cur;
				root = node;
			}
			else
			{
				node->prev = cur->prev;
				cur->prev->next = node;

				node->next = cur;
				cur->prev = node;
			}
        }

        void InsertAfter(size_t idx, T data)
        {
            if (idx >= Size())
			{
				throw("indexOutOfRange");
			}

			size_t count = 0;
			node_t cur = root;

			while (cur != nullptr && idx != count)
			{
				++count;
				cur = cur->next;
			}

			node_t node = node_t( new Node(data) );
                
			node->next = cur->next;
			node->prev = cur;
                
			if (cur->next != nullptr)
				cur->next->prev = node;

			cur->next = node;
        }

        void Remove(size_t idx)
        {
            if (idx >= Size())
            {
                throw("indexOutOfRange");
            }

            size_t count = 0;
            node_t cur = root;

            while (idx != count)
            {
                ++count;
                cur = cur->next;
            }

            if (cur->prev != nullptr)
            {
                cur->prev->next = cur->next;

                if (cur->next != nullptr)
                    cur->next->prev = cur->prev;
            }
            else
            {
                if (root->next != nullptr)
                {
                    root = root->next;
                    root->prev = nullptr;
                }
                else
                    root = nullptr;
            }
        }

        T Get(size_t idx) const
        {
            if (idx >= Size())
            {
                throw("indexOutOfRange");
            }

            size_t count = 0;
            node_t node = root;

            while (idx != count)
            {
                ++count;
                node = node->next;
            }

            return node->data;
        }

        size_t Size() const
        {
            
			size_t count = 0;
            node_t node = root;

            while (node != nullptr)
			{
				++count;
                node = node->next;
            }

			return count;
            
        }

        bool Empty() const
        {
			return root == nullptr;
        }

	private:

		node_t root;
    };

}